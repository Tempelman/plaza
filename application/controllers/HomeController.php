<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {
	
	public function __construct()
        {
                 parent::__construct();
                $this->load->helper('url');
                $this->load->model('HomeModel');
        }

	public function index()
	{
		$data['data'] = $this->HomeModel->get_author();
		$data['title']="Home";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu/home');
		$this->load->view('home/index');
		$this->load->view('templates/footer', $data);
	}
	
}
