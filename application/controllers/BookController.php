<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookController extends CI_Controller {
	
	public function __construct()
        {
                 parent::__construct();
                $this->load->helper('url');
                $this->load->model('BookModel');
        }
	public function books($id)
	{
		$data['data'] = $this->BookModel->get_book($id);
		$data['title']="Books";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu/books', $data);
		$this->load->view('books/index');
		$this->load->view('templates/footer', $data);
	}

	public function edit($book)
	{
		$data['data'] = $this->BookModel->edit_book($book);
		$data['title']="edit book";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu/edit', $data);
		$this->load->view('books/edit');
		$this->load->view('templates/footer');
	}

	public function editSave()
	{
		$update = array(
			"book_id" => $_POST["id"],
			"book_title" => $_POST["title"],
			"book_publisher" => $_POST["publisher"],
			"book_summary" => $_POST["summary"]
		);

		$this->BookModel->editSave_book($update);
		header('Location: /BookController/books/'.$_POST["author_id"]);
	}

	public function create(){
		$data['data']= $this->BookModel->get_authors();
		$data['title']="add book";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu/addh');
		$this->load->view('books/add');
		$this->load->view('templates/footer');

	}

		public function createSave()
	{
		$save = array(
			"author_id" => $_POST["cr_id"],
			"title" => $_POST["cr_book"],
			"publisher" => $_POST["cr_publisher"],
			"summary" => $_POST["cr_summary"]
		);

		$this->BookModel->createSave_book($save);
		header('Location: /BookController/books/'.$_POST["cr_id"]);
	}

	public function delete()
	{	
		$delete = array(
			"book_id" => $_POST["del_id"],
			"author" => $_POST["del_author"]
		);

		$this->BookModel->delete_book($delete);
		header('Location: /BookController/books/'.$_POST["del_author"]);
	}
}