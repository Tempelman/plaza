<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/modal.css') ?>">
	<script src="https://use.fontawesome.com/5e824e4b66.js"></script>

</head>
<body>

<div class="website">
	<div class="header">
		<h1 class="kopje"><?=$title?></h1>
		<img class="boekje" src="<?php echo base_url('public/img/book.gif')?>">	
	</div>
<!-- debug -->
<!-- <h1>Header</h1> -->