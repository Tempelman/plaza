    <div class="content"></div>
        <form class="formulier" name="newBook" method="post" action="<?php echo BASE_URL('BookController/createSave/')?>" onsubmit="return validateForm()" autocomplete="off">
            <p><label>Author name</label>

 			<select name="cr_id" required>
 			    <option value="0" disabled selected>Choose author</option>
                <?php foreach ($data as $key => $value) { ?> 
                    <option value="<?php echo $value->author_id; ?>"> <?php echo $value->author_name; ?> </option>
                <?php } ?>
 				</select></p>
            <p><label>Book title</label>
                <input required type="text" name="cr_book" placeholder="Title" /></p>

            <p><label>Publisher</label>
                <input required type="text" name="cr_publisher" placeholder="Publisher" /></p>

            <p><label>Enter a short summary</label>
                <textarea required class="largeip" type="text" name="cr_summary" placeholder="Enter a short summary...." cols="40" rows="7"></textarea></p>
					
					<label>&nbsp;</label>
					<input class="sent" type="submit" value="Submit">
        </form>
    </div>