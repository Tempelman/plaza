	<div class="content">
		<table>
			<tr>
				<th>Title</th>
				<th>Publisher</th>
				<th>Summary</th>
				<th colspan="2">Actions</th>
			</tr>

			<?php foreach ($data as $value) { ?>
				<tr>
					<td nowrap="true">	<?php echo $value->book_title; ?>	</td>
					<td nowrap="true">	<?php echo $value->book_publisher; ?>	</td>
					<td>	<?php echo $value->book_summary; ?>	</td>
					<td><a href="<?php echo BASE_URL('BookController/edit/') . $value->book_id ?>"> edit </a></td>
					<td><button class="btn" data-book-title="<?=$value->book_title?>" data-author-id="<?=$value->author_id?>" data-book-id="<?=$value->book_id?>">delete</button>	</a></td>
				</tr>
			<?php } ?>
		</table>
	</div>

	<div id="myModal" class="modal">
		<div class="modal-content">
			<h2><p>Are you sure you want to delete</p></h2>
				<h3><p class="nopad nomarg" id="modalTextTitle"></p><p class="nopad nomarg">?</p></h3>
				<form id="inputForm" method="post" action="<?php echo BASE_URL('BookController/delete/')?>" >
					<p class="btnline"><button class="modalbtn" type="submit">Yes delete it</button>
					<button class="modalbtn"href="" >No go back</button></p>


					<input id="inputAuthor" name="del_author" type="hidden" value="" />
					<input id="inputBookId" name="del_id" type="hidden" value="" />
        		</form>
		</div>
	</div>;