<?php
	class BookModel extends CI_Model {
			//En in de Model vang je het net zo op als in je Controller
		public function get_book($id){

			$this->load->database();
	        $data = $this->db->get_where('books', array("author_id" => $id));
	        return $data->result();

		}

		public function get_authors(){

			$this->load->database();
	        $data = $this->db->get('authors');
	        return $data->result();

		}

		public function edit_book($book){
			$this->load->database();
			$data = $this->db->get_where('books', array("book_id" => $book));
			return $data->row();
		}

		public function editSave_book($data){
			$this->load->database();
			$edit = array(
				
				"book_title" => $data['book_title'],
				"book_publisher" => $data['book_publisher'],
				"book_summary" => $data['book_summary']
				);
			$this->db->update('books',$edit,array("book_id" => $data['book_id']));
       }

		public function createSave_book($data){
			$this->load->database();
			$save = array(
				
				"author_id" => $data['author_id'],
				"book_title" => $data['title'],
				"book_publisher" => $data['publisher'],
				"book_summary" => $data['summary']
				);
			$this->db->INSERT('books',$save);
       }

   		public function delete_book($data){
			$this->load->database();
			$delete = array(
				
				"book_id" => $data['book_id'],
				"author" => $data['author']
				
			);

	        $this->db->DELETE('books',array("book_id" => $data['book_id']));

	    }

}
?>